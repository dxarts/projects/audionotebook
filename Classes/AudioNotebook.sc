AudioNotebook {
	var <dataPath;
	var pythonFile;
	var notebookPath;
	var commandWithArgs;
	var <samplesPath;
	var <files, dataPath2D, dataPath3D;
	classvar <>pythonCommand;

	*initClass {
		pythonCommand = Platform.userHomeDir ++ "/Tools/anaconda2/bin/python";
	}

	*new { |dataPath|
		^super.newCopyArgs(dataPath).init
	}

	init {

		notebookPath = File.realpath(AudioNotebook.filenameSymbol).dirname;
		notebookPath = File.realpath(notebookPath ++ "/../");

		// if path is relative, assume it should be relative to notebook path
		PathName(dataPath).isRelativePath.if({
			dataPath = notebookPath ++ "/data/" ++ dataPath ++ "/";
		});

		// check if dataPath exists
		File.exists(dataPath).not.if({ Error("Data path: " ++ dataPath ++ " does not exists").throw });

		samplesPath = dataPath ++ "samples/";

		dataPath2D = dataPath ++ "tsne/audioNotebook_2D.tsv";
		dataPath3D = dataPath ++ "tsne/audioNotebook_3D.tsv";

		// set path to python script
		pythonFile = notebookPath ++ "/AudioNotebook.py";
	}

	audioNotebook { |fftSize = 512, initial_dims = 32, perplexities = 32, doneAction|

		// make python command
		commandWithArgs = pythonCommand + pythonFile + dataPath + dataPath2D + dataPath3D + fftSize + initial_dims + perplexities;

		// post command for debugging
		commandWithArgs.postln;

		// run the command with user's doneAction
		commandWithArgs.unixCmd(doneAction);

	}

	points { |dim, center = true|
		var points, path;
		path = (dim == 2).if({ dataPath2D }, { dataPath3D });
		points = TabFileReader.read(path).asFloat;
		center.if({ points = points.linlin(0.0, 1.0, -1.0, 1.0) });
		^points
	}

	testPlot { |dim = 3|
		var window, buffers;
		var synths, points;
		var screenBounds, notesView;
		var server = Server.default;

		files = FileReader.read(dataPath ++ "filenames.txt", delimiter: $\n)[0];

		server.options.numBuffers_(files.size.nextPowerOfTwo);
		server.newAllocators;

		server.waitForBoot({

			screenBounds = Window.screenBounds;
			window = Window.new("MapTest", screenBounds);
			window.view.background_(Color.white);

			buffers = files.collect{ |path|
				CtkBuffer(path).load
			};

			points = this.points(dim).collect(_.asCartesian);

			notesView = PointView.new(window, window.bounds).points_(points).showIndices_(false).rotate_(0).tumble_(0.45pi).pointColors_(Color.black);

			window.front;

			synths = CtkProtoNotes(
				SynthDef('playBuf', {
					arg buffer, outbus, dur;
					var src, env;
					env = EnvGen.kr(Env([0, 1, 0], [0.01, dur - 0.02, 0.01], 'sin'));
					src = PlayBuf.ar(buffers[0].numChannels, buffer, BufRateScale.kr(buffer));
					Out.ar(outbus, src * env)
				})
			);

			notesView.userView.mouseDownAction_({
				|v,x,y, modifiers, buttonNumber, clickCount|
				var buffer, dur, nearestIndex, distances, mouseDownPnt;

				notesView.removeHighlight;
				mouseDownPnt = x@y;
				distances = notesView.pointsPx.collect{arg point;
					point.asPoint.dist(mouseDownPnt)
				};
				nearestIndex = distances.minIndex;
				notesView.pointsPx[nearestIndex];
				notesView.highlightPoints([nearestIndex]);
				buffer = buffers[nearestIndex];
				dur = buffer.duration;
				synths['playBuf'].note(duration: dur)
				.buffer_(buffer)
				.dur_(dur)
				.outbus_(0)
				.play;
			});

			notesView.userView.mouseMoveAction_({
				|v,x,y, modifiers, buttonNumber, clickCount|
				var buffer, dur, nearestIndex, distances, mouseDownPnt;

				notesView.removeHighlight;
				mouseDownPnt = x@y;
				distances = notesView.pointsPx.collect{arg point;
					point.asPoint.dist(mouseDownPnt)
				};
				nearestIndex = distances.minIndex;
				notesView.pointsPx[nearestIndex];
				notesView.highlightPoints([nearestIndex]);
				buffer = buffers[nearestIndex];
				dur = buffer.duration;
				synths['playBuf'].note(duration: dur)
				.buffer_(buffer)
				.dur_(dur)
				.outbus_(0)
				.play;
			});

			window.onClose_({
				CmdPeriod.run;
				buffers.do(_.free)
			});


		})
	}
}