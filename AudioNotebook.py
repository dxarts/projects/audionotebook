import sys
from utils import *
from tqdm import *
from os.path import join
from skimage.measure import block_reduce
from multiprocessing import Pool
import numpy as np
import scipy as scp
import librosa
from time import time
import itertools

# data_root = 'data/Snare/'
# data_root = 'data/Charango/'
# data_root = 'data/Voz/'
data_root = sys.argv[1]
data_path_2D = sys.argv[2]
data_path_3D = sys.argv[3]

n_fft = int(sys.argv[4])
hop_length = n_fft/2
use_logamp = False # boost the brightness of quiet sounds
reduce_rows = 10 # how many frequency bands to average into one
reduce_cols = 1 # how many time steps to average into one
crop_rows = 32 # limit how many frequency bands to use
crop_cols = 32 # limit how many time steps to use

initial_dims = [int(sys.argv[5])]
perplexities = [int(sys.argv[6])]
mode = 'fingerprints'

limit = None # set this to 100 to only process 100 samples

files = list(list_all_files(join(data_root, 'samples'), ['.wav', '.aif', '.aiff']))

np.savetxt(join(data_root, 'filenames.txt'), files, fmt='%s')
window = np.hanning(n_fft)
def job(fn):
    y, sr = librosa.load(fn, sr=None)
    S = librosa.stft(y, n_fft=n_fft, hop_length=hop_length, window=window)
    amp = np.abs(S)
    if reduce_rows > 1 or reduce_cols > 1:
        amp = block_reduce(amp, (reduce_rows, reduce_cols), func=np.mean)
    if amp.shape[1] < crop_cols:
        amp = np.pad(amp, ((0, 0), (0, crop_cols-amp.shape[1])), 'constant')
    amp = amp[:crop_rows, :crop_cols]
    if use_logamp:
        amp = librosa.logamplitude(amp**2)
    amp -= amp.min()
    if amp.max() > 0:
        amp /= amp.max()
    amp = np.flipud(amp) # for visualization, put low frequencies on bottom
    return amp
# def job(fn):
#     y, sr = librosa.load(fn, sr=None)
#     S, phase = librosa.magphase(librosa.stft(y, n_fft=n_fft, hop_length=hop_length, window=window))
#     kurt = scp.stats.kurtosis(S)
#     skew = scp.stats.skew(S)
#     # spectral flux (we need to define this as a function)
#     sf = np.sqrt(np.sum(np.diff(np.abs(S))**2, axis=0)) / (n_fft * 0.5)
#     # pad with zero at the beginning
#     sf = np.append(np.zeros(1), sf)
#     centroid = librosa.feature.spectral_centroid(S=S)
#     bandwidth = librosa.feature.spectral_bandwidth(S=S)
#     rolloff = librosa.feature.spectral_rolloff(S=S)
#     zc = librosa.feature.zero_crossing_rate(y, frame_length=n_fft, hop_length=hop_length)
#     rms = librosa.feature.rmse(S=S)
#     all = np.vstack((kurt, skew, sf, centroid, bandwidth, rolloff, zc, rms))
#     # number of features
#     M = 8
#     # find resampling ratio for columns
#     # we want a M x M matrix, we apply mean function to get it
#     ratio = int((y.size/hop_length) / M)
#     ratio = max(ratio, 1)
#     all = block_reduce(all, (1, ratio), func=np.mean)
#     # make sure we crop matrix to M x M
#     all = all[:M, :M]
# #     print(M, y.size/hop_length, ratio, all.shape)
#     return all

pool = Pool()
fingerprints = pool.map(job, files[:limit])
#print(fingerprints)
fingerprints = np.asarray(fingerprints).astype(np.float32)

np.save(join(data_root, 'fingerprints_spectral.npy'), fingerprints)

def save_tsv(data, fn):
    np.savetxt(fn, data, fmt='%.5f', delimiter='\t')
def tsne(data, data_path_2D, data_path_3D, prefix, initial_dims=30, perplexity=30):
    mkdir_p(data_root + 'tsne')

    X_2d = list(bh_tsne(data, initial_dims=initial_dims, perplexity=perplexity, no_dims=2, verbose=True))
    X_2d = normalize(np.array(X_2d))
    save_tsv(X_2d, data_path_2D)

    X_3d = list(bh_tsne(data, initial_dims=initial_dims, perplexity=perplexity, no_dims=3))
    X_3d = normalize(np.array(X_3d))
    save_tsv(X_3d, data_path_3D)

fingerprints = fingerprints.reshape(len(fingerprints), -1)
data = fingerprints

data = data.astype(np.float64)
def job(params):
    start = time()
    tsne(data, data_path_2D, data_path_3D, mode, initial_dims=params[0], perplexity=params[1])
    print 'initial_dims={}, perplexity={}, {} seconds'.format(params[0], params[1], time() - start)
params = list(itertools.product(initial_dims, perplexities))
print(params)
pool = Pool()
pool.map(job, params)
